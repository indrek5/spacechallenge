public class U2 extends Rocket {

    @Override
    public boolean launch() {
        double launchFailurePrc = (0.04*(currentWeight-weight)/(maxWeight-weight));
        return !(Math.random() < launchFailurePrc);
    }

    @Override
    public boolean land() {
        double landFailurePrc = (0.08*(currentWeight-weight)/(maxWeight-weight));
        return !(Math.random() < landFailurePrc);
    }
    U2(){
        cost = 120;
        weight = 18000;
        maxWeight = 29000;
        currentWeight = weight;
    }
}

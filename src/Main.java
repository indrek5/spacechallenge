import java.io.FileNotFoundException;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) throws FileNotFoundException {

        Simulation simulation = new Simulation();
        int totalU1BudgetPhase1 = 0, totalU1BudgetPhase2 = 0, totalU2BudgetPhase1 = 0, totalU2BudgetPhase2 = 0;

        ArrayList<Item> phase1Items = simulation.loadItems("phase-1.txt");
        ArrayList<Item> phase2Items = simulation.loadItems("phase-2.txt");

        ArrayList<Rocket> U1phase1Rockets = simulation.loadU1(phase1Items);
        ArrayList<Rocket> U1phase2Rockets = simulation.loadU1(phase2Items);
        ArrayList<Rocket> U2phase1Rockets = simulation.loadU2(phase1Items);
        ArrayList<Rocket> U2phase2Rockets = simulation.loadU2(phase2Items);


        int repeat = 10000;
        for (int i = 1; i <= repeat; i++) {
            int U1BudgetPhase1 = simulation.RunSimulation(U1phase1Rockets);
            totalU1BudgetPhase1 += U1BudgetPhase1;

            int U1BudgetPhase2 = simulation.RunSimulation(U1phase2Rockets);
            totalU1BudgetPhase2 += U1BudgetPhase2;

            int U2BudgetPhase1 = simulation.RunSimulation(U2phase1Rockets);
            totalU2BudgetPhase1 += U2BudgetPhase1;

            int U2BudgetPhase2 = simulation.RunSimulation(U2phase2Rockets);
            totalU2BudgetPhase2 += U2BudgetPhase2;
        }
        System.out.println("Mission iterations: " + repeat);
        System.out.println("U1 phase-1 avg. budget = " + totalU1BudgetPhase1/repeat + "; Avg. no of rockets per mission: " + U1phase1Rockets.size());
        System.out.println("U1 phase-2 avg. budget = " + totalU1BudgetPhase2/repeat + "; Avg. no of rockets per mission: " + U1phase2Rockets.size());
        System.out.println();
        System.out.println("U2 phase-1 avg. budget = " + totalU2BudgetPhase1/repeat + "; Avg. no of rockets per mission: " + U2phase1Rockets.size());
        System.out.println("U2 phase-2 avg. budget = " + totalU2BudgetPhase2/repeat + "; Avg. no of rockets per mission: " + U2phase2Rockets.size());

    }
}

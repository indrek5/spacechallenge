import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.ArrayList;

class Simulation {
    ArrayList<Item> loadItems(String fileName) throws FileNotFoundException{
        File file = new File(fileName);
        Scanner scanner = new Scanner(file);
        ArrayList<Item> items = new ArrayList<>();
        while (scanner.hasNextLine()) {
            String[] line = scanner.nextLine().split("=");
            Item item = new Item();
            item.name = line[0];
            item.weight = Integer.parseInt(line[1]);
            items.add(item);
        }
        return items;
    }

    ArrayList<Rocket> loadU1(ArrayList<Item> loadList) {

        ArrayList<Rocket> U1List = new ArrayList<>();
        int itemCounter = 0;
        while (itemCounter<loadList.size()) {
            U1 newU1 = new U1();
            for (int i=itemCounter; i < loadList.size(); i++, itemCounter++) {
                if (newU1.canCarry(loadList.get(i))) {
                    newU1.carry(loadList.get(i));
                } else break;
            }
            U1List.add(newU1);
        }
        return U1List;
    }


    ArrayList<Rocket> loadU2(ArrayList<Item> loadList) {

        ArrayList<Rocket> U2List = new ArrayList<>();
        int itemCounter = 0;
        while (itemCounter<loadList.size()) {
            U2 newU2 = new U2();
            for (int i=itemCounter; i < loadList.size(); i++, itemCounter++) {
                if (newU2.canCarry(loadList.get(i))) {
                    newU2.carry(loadList.get(i));
                } else break;
            }
            U2List.add(newU2);
        }
        return U2List;
    }


    int RunSimulation(ArrayList<Rocket> rocketList) {
        int budget = 0;
        for (Rocket rocket : rocketList) {
            budget += rocket.cost;
            while (!rocket.launch() || !rocket.land()) {
                budget += rocket.cost;
            }
        }
        return budget;
    }
}
public class U1 extends Rocket {

    @Override
    public boolean launch() {
        double launchFailurePrc = (0.05*(currentWeight-weight)/(maxWeight-weight));
        return (Math.random() > launchFailurePrc);
    }

    @Override
    public boolean land() {
        double landFailurePrc = (0.01*(currentWeight-weight)/(maxWeight-weight));
        return (Math.random() > landFailurePrc);
    }
    U1(){
        cost = 100;
        weight = 10000;
        maxWeight = 18000;
        currentWeight = weight;
    }
}
